/*
 * Public API Surface of ng-research-lib
 */

export * from './lib/ng-research-lib.service';
export * from './lib/ng-research-lib.component';
export * from './lib/header/header.component';
export * from './lib/ng-research-lib.module';
