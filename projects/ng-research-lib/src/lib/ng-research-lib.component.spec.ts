import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgResearchLibComponent } from './ng-research-lib.component';

describe('NgResearchLibComponent', () => {
  let component: NgResearchLibComponent;
  let fixture: ComponentFixture<NgResearchLibComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NgResearchLibComponent]
    });
    fixture = TestBed.createComponent(NgResearchLibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
