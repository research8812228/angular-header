import { NgModule } from '@angular/core';
import { NgResearchLibComponent } from './ng-research-lib.component';
import { HeaderComponent } from './header/header.component';



@NgModule({
  declarations: [
    NgResearchLibComponent,
    HeaderComponent
  ],
  imports: [
  ],
  exports: [
    NgResearchLibComponent,
    HeaderComponent
  ]
})
export class NgResearchLibModule { }
