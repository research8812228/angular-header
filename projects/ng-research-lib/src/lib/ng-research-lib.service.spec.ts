import { TestBed } from '@angular/core/testing';

import { NgResearchLibService } from './ng-research-lib.service';

describe('NgResearchLibService', () => {
  let service: NgResearchLibService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NgResearchLibService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
